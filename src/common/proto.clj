(ns common.proto
  (:require [flatland.protobuf.core :as pb]
            [clojurewerkz.support.bytes :refer [to-byte-array]])
  (:import flatland.protobuf.PersistentProtocolBufferMap$Def))

(def ^:private name-strategy 
  {:naming-strategy PersistentProtocolBufferMap$Def/convertUnderscores})

(def ^:private protodefs (atom {}))

(defn protodef 
  "Fix from issue 45, see https://github.com/flatland/clojure-protobuf/issues/45"
  [type]
  (let [pd (pb/protodef type name-strategy)]
    (swap! protodefs assoc (.getFullName pd) pd)
    pd))

(defn proto-from-name [name]
  (get @protodefs name))

(defn parse-protobuf [{:keys [type]} payload]
  (when-let [pd (proto-from-name type)]
    (with-meta (pb/protobuf-load pd payload) {:type pd})))

;; RabbitMQ functions, extensions etc.
;; automatically serialize to protobuf
(extend-protocol clojurewerkz.support.bytes/ByteSource
  flatland.protobuf.PersistentProtocolBufferMap
  (to-byte-array [pbm] (to-byte-array (pb/protobuf-dump pbm))))