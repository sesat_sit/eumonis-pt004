(ns common.protorabbit
  (:require 
    [flatland.protobuf.core :as pb :refer [protobuf protobuf?]]
    [langohr [basic :as lb]]))

(defn send! [ch {:keys [correlation-id reply-to send-to] :as m} msg]
  (let [type (or (:type (meta msg)) (.getMessageType msg)) 
        type-name (.getFullName type)
        target-key (or send-to reply-to)
        payload (if (protobuf? msg) msg (protobuf type msg))]
    (when target-key
      (lb/publish ch "" target-key payload 
                  :correlation-id correlation-id
                  :reply-to reply-to
                  :content-type "application/x-protobuf"
                  :type type-name))))