(ns routing.routing-test
  (:require [routing.contracts :as con]
            [routing.generator.routingkey :as gen]
            [routing.generator.io :as io]
            [common.rabbit :refer [with-connection]]
            [clojure.tools.logging :as log :refer [info infof trace warn]]
            [langohr 
             [core      :as rmq]
             [channel   :as lch]
             [exchange  :as le] 
             [queue     :as lq]
             [consumers :as lc]
             [basic     :as lb]
             [shutdown :as ls]]))

;;;;;;;;;; interactive test 
(defn send-msg! [ch user routing-key contract to payload]
  (lb/publish ch (str user "-ex-write") routing-key (.getBytes payload) :persistent true))


(defn test-routing-rk-multiple-vhosts [& [port]] 
  (let [port (or port 5672)] 
    (with-connection 
      [conn-si (rmq/connect {:vhost "VH_siemens" :port port})
       ch-si (lch/open conn-si)
       conn-tiq (rmq/connect {:vhost "VH_tiq" :port port})
       ch-tiq (lch/open conn-tiq)
       conn-cn (rmq/connect {:vhost "VH_company_n" :port port})
       ch-cn (lch/open conn-cn)
       conn  (rmq/connect {:vhost "ppu" :port port})
       ch (lch/open conn)] 
      (let [s (fn [ch n] (lc/subscribe ch (str n "-q-0") (fn [_ hdrs msg] (info (str n " got message: ") (String. msg) #_hdrs)) :auto-ack true))
            i-ca (lq/declare-server-named ch)]
        (lq/bind ch i-ca "invalid routing key") 
        (s ch-tiq "tiq")
        (s ch-si "siemens")
        (s ch-cn "company_n")
        (lc/subscribe ch i-ca (fn [_ hdrs msg] (info "INVALID rk: " (String. msg) hdrs)) :auto-ack true)
      
        (send-msg! ch-si "siemens" "siemens.tag_1.context_1"  "" "" "schreibe viele Daten, alles i.O.")
        (send-msg! ch-si "siemens" "siemens.tag_1"  "" "" "schreibe per Broadcast an alle, die Tag 'tag_1' empfangen duerfen")
        (send-msg! ch-si "siemens" "siemens.tag_2.context_multi"  "" "" "das hier schickt Siemens an alle, die im context 'context_multi' Daten speichern können (multicast)")
        (send-msg! ch-tiq "tiq" "tiq.COMMUNICATION.RPC.ANSWER.context_2" "" "" "Das sollte NUR an siemens gehen")
        (send-msg! ch-si "siemens" "siemens.tag_1.context_unbekannt" "" "" "dieser context existiert nicht")
        (send-msg! ch-si "siemens" "siemens.Foo.context_1" "" "" "diese Aktion ist nicht vorhanden")
        (Thread/sleep 1000)))))

(defn benchmark-routing-rk-multiple-vhosts [] 
  (with-connection [conn-si  (rmq/connect {:vhost "VH_siemens"})
                    ch-si (lch/open conn-si)
                    conn-tiq  (rmq/connect {:vhost "VH_tiq"})
                    ch-tiq (lch/open conn-tiq)
                    conn-cn  (rmq/connect {:vhost "VH_company_n"})
                    ch-cn (lch/open conn-cn)
                    conn  (rmq/connect {:vhost "ppu"})
                    ch (lch/open conn)] 
    (let [n 10000
          global-cdl (java.util.concurrent.CountDownLatch. n)
          local-cdl (atom nil)
          s (fn [ch n] (lc/subscribe ch (str n "-q-0") (fn [_ _ _] (.countDown global-cdl) (.countDown @local-cdl)) :auto-ack true))
          err (fn [ch q n] (lc/subscribe ch q (fn [_ _ _] (.countDown global-cdl)(.countDown @local-cdl)) :auto-ack true))
          i-ca (lq/declare-server-named ch)
          i-s (lq/declare-server-named ch)]
      (lq/bind ch i-ca "invalid tag") 
      (lq/bind ch i-s "invalid sender") 
      (s ch-tiq "tiq")
      (s ch-si "siemens")
      (s ch-cn "company_n")
      (err ch i-s "INVALID SENDER")
      (err ch i-ca "INVALID tag")
      
      (time 
        (while (not (zero? (.getCount global-cdl)))
          (reset! local-cdl (java.util.concurrent.CountDownLatch. 7))
          (send-msg! ch-si "siemens" "siemens.tag_1.context_1"  "" "" "schreibe viele Daten, alles i.O.")
          (send-msg! ch-si "siemens" "siemens.tag_2.context_multi"  "" "" "das hier schickt Siemens an alle, die im context 'context_multi' Daten speichern können (multicast)")
          (send-msg! ch-tiq "tiq" "tiq.COMMUNICATION.RPC.ANSWER.context_1" "" "" "Das sollte NUR an siemens gehen (trotz Multicast schickt sich niemand selbst antworten)")
          (send-msg! ch-si "siemens" "siemens.tag_1.context_unbekannt" "" "" "dieser context existiert nicht")
          (send-msg! ch-si "siemens" "siemens.Foo.context_1" "" "" "diese Aktion ist nicht vorhanden")
          (.await @local-cdl)))
      (Thread/sleep 1000))))

;;;;;;;; benchmarks

(defn benchmark-static-routing []
  (with-connection [conn  (rmq/connect {:vhost "gen-routes"})
                    ch (lch/open conn)]
    (let [starttime (atom 0)
          counter (atom 0)
          n 10000
          runtime (promise)]
      (lc/subscribe ch "tiq-q-0" 
                    (fn [_ _ _] 
                      (when (= n (swap! counter inc)) (deliver runtime (- (java.lang.System/currentTimeMillis) @starttime)))) :auto-ack true)
      (reset! starttime (java.lang.System/currentTimeMillis))
      (dotimes [_ n] (send-msg! ch "siemens" "DATA.SET.BULK"  "context_1" "all" "schreibe viele Daten, alles i.O."))      
      (double (/ (deref runtime) n))))) 

(defn benchmark-static-routing-rk []
  (with-connection [conn  (rmq/connect {:vhost "gen-routes2"})
                    ch (lch/open conn)] 
    (let [starttime (atom 0)
          counter (atom 0)
          n 10000
          runtime (promise)]
      (lc/subscribe ch "tiq-q-0" 
                    (fn [_ _ _] 
                      (when (= n (swap! counter inc)) (deliver runtime (- (java.lang.System/currentTimeMillis) @starttime)))) :auto-ack true)
      (reset! starttime (java.lang.System/currentTimeMillis))
      (dotimes [_ n] (send-msg! ch "siemens" "siemens.context_1.DATA.SET.BULK.tiq"  "" "" "schreibe viele Daten, alles i.O."))      
      (double (/ (deref runtime) n))))) 

 
;(pcalls benchmark-static-routing benchmark-dynamic-routing)
;=> (0.6644 0.193)
; dynamic routing mit direct exchanges ist etwa 3x-4x schneller, hängt also alles von der
; Performanz der Rule Engine ab bzw. Latenz des Netzwerks.


(defn test-cc-bcc [] 
  (with-connection [conn  (rmq/connect {:vhost "gen-routes"})
                    ch (lch/open conn)]
    (lc/subscribe ch "doit" (fn [_ {h :headers} msg] (info "got message from doit: " (String. msg) h)) :auto-ack true)
    (lc/subscribe ch "doitfoo" (fn [_ {h :headers} msg] (info "got message from doitfoo: " (String. msg) h)) :auto-ack true)
    (lc/subscribe ch "doit.foo" (fn [_ {h :headers} msg] (info "got message from doit.foo: " (String. msg) h)) :auto-ack true)
    (lc/subscribe ch "doitbar" (fn [_ {h :headers} msg] (info "got message from doit.bar: " (String. msg) h)) :auto-ack true)      
    (lc/subscribe ch "doit.bar" (fn [_ {h :headers} msg] (info "got message from doit.bar: " (String. msg) h)) :auto-ack true)      
    
    (lb/publish ch "" "doit" (.getBytes "doit") :persistent true) 
    (lb/publish ch "" "doit" (.getBytes "doit cc foo") :persistent true :headers {"CC" ["doit.foo"]}) 
    (lb/publish ch "" "doit" (.getBytes "doit cc bar") :persistent true :headers {"CC" ["doit.bar"]}) 
    (lb/publish ch "" "doit" (.getBytes "doit cc foo bar") :persistent true :headers {"BCC" ["doit.foo" "doit.bar"]})       
    
    (Thread/sleep 2000)))

(defn test-wildcards [] 
  (with-connection [conn  (rmq/connect {:vhost "gen-routes"})
                    ch (lch/open conn)] 
    (let [q1 (lq/declare-server-named ch)
          q2 (lq/declare-server-named ch)]
      (le/declare ch "foo" "topic" :auto-delete true)
      (lq/bind ch q1 "foo" :routing-key "FOO") 
      (lq/bind ch q1 "foo" :routing-key "FOO.*")
      
      (lc/subscribe ch q1 (fn [_ {h :headers} msg] (info "FOO: " (String. msg) h)) :auto-ack true)
      (lc/subscribe ch q2 (fn [_ {h :headers} msg] (info "FOO.*: " (String. msg) h)) :auto-ack true)
      
      (lb/publish ch "foo" "FOO" (.getBytes "FOO")) 
      (lb/publish ch "foo" "FOO.bar" (.getBytes "FOO.bar")) 
      
      (Thread/sleep 2000))))

(defn binding-perf-test [vhost type binder-fn publish-fn]
  (with-connection [conn (rmq/connect {:vhost vhost})
                    ch (lch/open conn)] 
    (let [q (lq/declare-server-named ch)
          ex-src (str type "-ex-source")
          ex-target (str type "-ex-target")
          n 100000
          step 100
          per-iteration 20
          t (atom 0)
          counter (atom 0)]
    ; declare exchanges
      (le/declare ch ex-src type :durable false :auto-delete true)
      (le/declare ch ex-target "fanout" :durable false :auto-delete true)
      ; add bindings
      (dotimes [n n]
        (binder-fn ch ex-target ex-src n))
      ; add and subscribe to queue
      (lq/bind ch q ex-target)      
      (lc/subscribe ch q (fn [_ _ msg] 
                           (swap! counter inc)
                           (when (= per-iteration @counter)
                             (reset! counter 0)
                             (infof "Type: %s Msg: %s in %.2f ms/roundtrip" type (String. msg) (/ (/ (- (System/nanoTime) @t) per-iteration) 1000000.0))))
                    :auto-ack true)
      ; run tests
      (doseq [n (range 0 n step)] 
        (reset! t (java.lang.System/nanoTime))
        (dotimes [_ per-iteration] 
          (publish-fn ch ex-src n))
        (Thread/sleep 100))
      ; wait until done
      (Thread/sleep 10000))))

(defn binding-perf-test-topic [vhost]
  (binding-perf-test 
    vhost 
    "topic"
    (fn [ch ex-target ex-src n] (le/bind ch ex-target ex-src :routing-key (str "rk-" n ".#")))
    (fn [ch ex-src n] (lb/publish ch ex-src (str "rk-" n ".foo") (str "r=" n)))))

(defn binding-perf-test-direct [vhost]
  (binding-perf-test 
    vhost 
    "direct"
    (fn [ch ex-target ex-src n] (le/bind ch ex-target ex-src :routing-key (str "rk-" n)))
    (fn [ch ex-src n] (lb/publish ch ex-src (str "rk-" n) (str "r=" n)))))

(defn binding-perf-test-headers [vhost]
  (binding-perf-test 
    vhost 
    "headers"
    (fn [ch ex-target ex-src n] (le/bind ch ex-target ex-src :arguments {"rk" (str "rk-" n) "x-match" "all"}))
    (fn [ch ex-src n] (lb/publish ch ex-src "" (str "r=" n) :headers {"rk" (str "rk-" n)}))))

(defn test-msg-ram-load [vhost]
  (with-connection [conn (rmq/connect {:vhost vhost})
                    ch (lch/open conn)]
    (dotimes [_ 100]
      (lq/bind ch (lq/declare-server-named ch) "amq.fanout"))
    (lb/publish ch "amq.fanout" "whatever" (byte-array 100000000) :persistent true)
    (Thread/sleep 100000)))

(defn test-headers-userid-matching [vhost]
  (with-connection [conn (rmq/connect {:vhost vhost})
                    ch (lch/open conn)]
    (let [ex "amq.headers"
          q "meine queue"]
      (lq/declare ch q)
      (lq/bind ch q ex :arguments {"user_id" "guest" "x-match" "all"})
      (lc/subscribe ch q (fn [_ h msg] (info "got msg: " (String. msg) h)) :auto-ack true)
      (lb/publish ch ex "" (.getBytes "test with properties only") :user-id "guest")
      (lb/publish ch ex "" (.getBytes "test with headers") :user-id "guest" :headers {"user_id" "guest"})
      (Thread/sleep 1000))))

(defn prio-test []
  (with-connection [conn (rmq/connect)
                    ch (lch/open conn)]
    (dotimes [_ 100000] (lb/publish ch "prio-test" "foo" (.getBytes "no prio"))
    (lb/publish ch "prio-test" "foo" (.getBytes "prio 0") :priority 0)
    (lb/publish ch "prio-test" "foo" (.getBytes "prio 3") :priority 3)
    (lb/publish ch "prio-test" "foo" (.getBytes "prio 5") :priority 5))))