(ns routing.cebit
  (:require [routing.contracts :refer [pw]]
            [routing.generator.rabbit-password :as rp]))

(def ^:private shovel-pw "shovel")
(def ^:private shovel-pw-hash (rp/rabbit-password-hash shovel-pw (byte-array (map byte [1 2 3 4]))))

(def contracts-zone-pf-area-web 
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Plattform, Area Web"
    :aliases ["rabbit@VM-PF-W-CCU" "10.1.1.2:15673" "10.1.1.2:15672"] 
    :management-user "admin"
    :management-password "tgsF52Pf5irMiDormEXO"
    :management-url "http://10.1.1.2:15673"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}    
  {:users {"tiq" {:name "tiq" 
                  :password (pw "tiq")
                  :queues #{"tiq-q-0"}
                  :exchange "tiq-ex-write"
                  :allocations {"dss1" #{"tiq-q-0"}
                                "dss2" #{"tiq-q-0"}
                                "erp3t" #{"tiq-q-0"}
                                "erp4t" #{"tiq-q-0"}
                                "erp5" #{"tiq-q-0"}
                                "erp6t" #{"tiq-q-0"}}} 
            "siemens" {:name "siemens"
                       :password (pw "siemens")
                       :queues #{"siemens-q-0"} 
                       :exchange "siemens-ex-write"
                       :allocations {"erp4" #{"siemens-q-0"}
                                     "erp6" #{"siemens-q-0"}
                                     "das2" #{"siemens-q-0"}}} 
            "ul" {:name "ul" 
                  :password (pw "ul")
                  :queues #{"ul-q-0"}
                  :exchange "ul-ex-write"
                  :allocations {}}
            "psipenta" {:name "psipenta" 
                        :password (pw "psipenta")
                        :queues #{"psipenta-q-0"}
                        :exchange "psipenta-ex-write"
                        :allocations {"erp1" #{"psipenta-q-0"}
                                      "erp3" #{"psipenta-q-0"}}}}
   :covenants {"dss1" {:from "siemens" 
                       :to "tiq" 
                       :tag "storedata"}
               "dss2" {:from "ul" 
                       :to "tiq" 
                       :tag "storedata"}
               "erp1" {:from "siemens" 
                       :to "psipenta" 
                       :tag "offer"}
               "erp6" {:from "psipenta" 
                       :to "siemens" 
                       :tag "order"}
               "erp6t" {:from "psipenta" 
                        :to "tiq" 
                        :tag "order"}               
               "erp3" {:from "siemens" 
                       :to "psipenta" 
                       :tag "order"}
               "erp3t" {:from "siemens" 
                        :to "tiq" 
                        :tag "order"}
               "erp4" {:from "psipenta" 
                       :to "siemens" 
                       :tag "report"}
               "erp4t" {:from "psipenta" 
                        :to "tiq" 
                        :tag "report"}
               "erp5" {:from "psipenta" 
                       :to "tiq" 
                       :tag "bill"}
               "das2" {:from "ul" 
                       :to "siemens" 
                       :tag "diagnosis"}} 
         :collections {"ALL" #{"dss1" "dss2" "erp1" "erp3" "erp3t" "erp4" "erp4t" "das2" "erp5" "erp6" "erp6t"}}})


#_(def contracts-zone-siemens-area-web-localusers 
  "combination of proxy user for remote users in an upstream instance of rabbitmq
and delegation of covenants from one platform user to its local users."
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Plattform, Area Web"
    :aliases ["rabbit@VM-SI-W-CCU" "10.1.2.2:15673"]
    :management-user "admin"
    :management-password "pDBQd7uS6llbDwYFOWzF"
    :management-url "http://10.1.2.2:15673"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}    
  {:users {"proxyuser" {:name "proxyuser" 
                        :password (pw "proxyuser")
                        :queues #{"proxyuser-q-0"}
                        :exchange "proxyuser-ex-write" 
                        :allocations {"2" #{"proxyuser-q-0"}
                                      "3" #{"proxyuser-q-0"}
                                      "7" #{"proxyuser-q-0"}
                                      "8" #{"proxyuser-q-0"}}
                        :remote {:aliases #{"tiq" "ul" "psipenta"}
                                 :uri "amqp://siemens:siemens@localhost:5672/VH_siemens"
                                 :exchange "siemens-ex-write" 
                                 :queue "siemens-q-0"}}
           "siemens" {:name "siemens"
                      :password (pw "siemens")
                      :exchange "siemens-ex-write"
                      :queues #{"siemens-q-0" "siemens-q-1"}
                      :allocations {"1" #{"siemens-q-1"}
                                    "4" #{"siemens-q-1"}
                                    "5" #{"siemens-q-1"}
                                    "6" #{"siemens-q-0"}}
                      :localusers {"winccoa" {:name "winccoa" 
                                              :password (pw "winccoa")
                                              :queues #{"siemens-q-0"}
                                              :exchange "winccoa-ex-write"
                                              :delegation #{"7" "8"}}
                                   "comos" {:name "comos" 
                                            :password (pw "comos")
                                            :queues #{"siemens-q-1"}
                                            :exchange "comos-ex-write"
                                            :delegation #{"3" "2"}}}}}
   :covenants {"1" {:from "winccoa" 
                    :to "comos" 
                    :tag "alert"}
               "2" {:from "siemens" 
                    :to "psipenta" 
                    :tag "offer"}
               "3" {:from "siemens" 
                    :to "psipenta" 
                    :tag "order"}
               "4" {:from "psipenta" 
                    :to "siemens" 
                    :tag "offer"}
               "5" {:from "psipenta" 
                    :to "siemens" 
                    :tag "report"}
               "6" {:from "ul" 
                    :to "siemens" 
                    :tag "diagnosis"}
               "7" {:from "siemens" 
                    :to "tiq" 
                    :tag "storedata"}
               "8" {:from "siemens" 
                    :to "ul" 
                    :tag "storedata"}} 
   :collections {"ALL" #{"1" "2" "3" "4" "5" "6" "7" "8"}}})

;{:ppu-vhost "VH_ppu" 
;    :name "Zone Siemens, Area Web"
;    :aliases ["rabbit@VM-SI-W-CCU" "10.1.2.2:15673"]
;    :management-user "admin"
;    :management-password "pDBQd7uS6llbDwYFOWzF"
;    :management-url "http://10.1.2.2:15673"
;    :shovel-user "shovel"
;    :shovel-password shovel-pw
;    :shovel-password-hash shovel-pw-hash}

(def contracts-zone-siemens-area-web 
  "combination of proxy user for remote users in an upstream instance of rabbitmq
and delegation of covenants between platform users."
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Siemens, External"
    :aliases ["rabbit@VM-SI-W-CCU" "10.0.0.10:15673" "10.0.0.10:5672"]
    :management-user "admin"
    :management-password "pDBQd7uS6llbDwYFOWzF"
    :management-url "http://10.0.0.10:15672"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}  
  {:users {"proxy-pf-si" {:name "proxy-pf-si" 
                        :password (pw "proxy-pf-si")
                        :queues #{"proxy-pf-si-q-0"}
                        :exchange "proxy-pf-si-ex-write" 
                        :allocations {"2" #{"proxy-pf-si-q-0"}
                                      "3" #{"proxy-pf-si-q-0"}
                                      "7" #{"proxy-pf-si-q-0"}}
                        :remote {:aliases #{"tiq" "ul" "psipenta"}
                                 :uri "amqp://siemens:siemens@10.1.1.2:15672/VH_siemens"
                                 :local-uri "amqp://proxy-pf-si:proxy-pf-si@/VH_proxy-pf-si" 
                                 :exchange "siemens-ex-write" 
                                 :queue "siemens-q-0"}}
           "siemens" {:name "siemens"
                      :password (pw "siemens")
                      :exchange "siemens-ex-write"
                      :queues #{"siemens-q-0" "siemens-q-1"}
                      :allocations {;"1" #{"siemens-q-1"} ;no need for allocations, these message got delegated!
                                    ;"4" #{"siemens-q-1"}
                                    ;"5" #{"siemens-q-1"}
                                    ;"6" #{"siemens-q-0"}
                                    ;"erp6" #{"siemens-q-1"}
                                    }}
           "winccoa" {:name "winccoa" 
                      :password (pw "winccoa")
                      :queues #{"winccoa-q-0" "winccoa-q-1"}
                      :exchange "winccoa-ex-write"
                      :allocations {"6" #{"winccoa-q-1"}
                                    "1x" #{"winccoa-q-0"}}
                      :delegation {"siemens" #{"6" "7"}}}
           "comos" {:name "comos" 
                    :password (pw "comos")
                    :queues #{"comos-q-0" "comos-q-1"}
                    :exchange "comos-ex-write"
                    :allocations {"1" #{"comos-q-0"}
                                  "4" #{"comos-q-1"}
                                  "5" #{"comos-q-1"}
                                  "erp6" #{"comos-q-1"}}
                    :delegation {"siemens" #{"1" "2" "3" "4" "5" "erp6"}}}}
   :covenants {"1" {:from "winccoa" 
                    :to "comos" 
                    :tag "alert"}
               "1x" {:from "comos" 
                    :to "winccoa" 
                    :tag "alertrequest"}
               "2" {:from "siemens" 
                    :to "psipenta" 
                    :tag "offer"}
               "3" {:from "siemens" 
                    :to "psipenta" 
                    :tag "order"}
               "4" {:from "psipenta" 
                    :to "siemens" 
                    :tag "offer"}
               "erp3t" {:from "siemens" 
                        :to "tiq" 
                        :tag "order"}               
               "5" {:from "psipenta" 
                    :to "siemens" 
                    :tag "report"}
               "6" {:from "ul" 
                    :to "siemens" 
                    :tag "diagnosis"}
               "7" {:from "siemens" 
                    :to "tiq" 
                    :tag "storedata"}
               "erp6" {:from "psipenta" 
                       :to "siemens" 
                       :tag "order"}} 
   :collections {"ALL" #{"1" "2" "3" "4" "5" "6" "7" "1x" "erp6" "erp3t"}}})

(def contracts-zone-siemens-area-web-hmi 
  "combination of proxy user for remote users in an upstream instance of rabbitmq
and delegation of covenants between platform users."
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Siemens, External"
    :aliases ["rabbit@VM-SI-W-CCU" "127.0.0.1:15672" "127.0.0.1:5672"]
    :management-user "guest"
    :management-password "guest"
    :management-url "http://127.0.0.1:15674"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}  
  {:users {"winccoa" {:name "winccoa" 
                      :password (pw "winccoa")
                      :queues #{"winccoa-q-0" "winccoa-q-1"}
                      :exchange "winccoa-ex-write"
                      :allocations {"1x" #{"winccoa-q-0"}}}
           "comos" {:name "comos" 
                    :password (pw "comos")
                    :queues #{"comos-q-0" "comos-q-1"}
                    :exchange "comos-ex-write"
                    :allocations {"1" #{"comos-q-0"}}}}
   :covenants {"1" {:from "winccoa" 
                    :to "comos" 
                    :tag "alert"}
               "1x" {:from "comos" 
                    :to "winccoa" 
                    :tag "alertrequest"}} 
   :collections {"ALL" #{"1" "1x"}}})

(def contracts-zone-ul-area-web
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Uni, Area Web"
    :aliases ["rabbit@VM-UL-W-CCU" "10.1.4.2:15673" "192.168.0.10:5672"] 
    :management-user "admin"
    :management-password "z1sLat202lt3mR7B2Uyn"
    :management-url "http://10.1.4.2:15673"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}
  {:users {"proxy-pf-ul"{:name "proxy-pf-ul" 
                      :password (pw "proxy-pf-ul")
                      :queues #{"proxy-pf-ul-q-0"}
                      :exchange "proxy-pf-ul-ex-write" 
                      :allocations {"dss2" #{"proxy-pf-ul-q-0"}
                                    "das2" #{"proxy-pf-ul-q-0"}}
                      :remote {:aliases #{"tiq" "siemens"}
                               :uri "amqp://ul:ul@10.1.1.2:15672/VH_ul"
                               :local-uri "amqp://proxy-pf-ul:proxy-pf-ul@/VH_proxy-pf-ul"
                               :exchange "ul-ex-write"
                               :queue "ul-q-0"}}
           "ul" {:name "ul" 
                 :password (pw "ul")
                 :queues #{"ul-q-0"}
                 :exchange "ul-ex-write"
                 :allocations {}}}
   :covenants {"dss2" {:from "ul" 
                     :to "tiq" 
                     :tag "storedata"}
               "das2" {:from "ul" 
                       :to "siemens" 
                       :tag "diagnosis"}} 
         :collections {"ALL" #{"dss2" "das2"}}})

(def contracts-zone-ul-area-app
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Uni, Area Application"
    :aliases ["rabbit@VM-UL-A-CCU" "10.1.4.2:25673"]
    :management-user "admin"
    :management-password "z1sLat202lt3mR7B2Uyn"
    :management-url "http://10.1.4.2:25673"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}
  {:users {"proxy-web-ul"{:name "proxy-web-ul" 
                      :password (pw "proxy-web-ul")
                      :queues #{"proxy-web-ul-q-0"}
                      :exchange "proxy-web-ul-ex-write" 
                      :allocations {"dss2" #{"proxy-web-ul-q-0"}
                                    "das2" #{"proxy-web-ul-q-0"}}
                      :remote {:aliases #{"tiq" "siemens"}
                               :uri "amqp://ul:ul@192.168.0.10:5672/VH_ul"
                               :local-uri "amqp://proxy-web-ul:proxy-web-ul@/VH_proxy-web-ul"
                               :exchange "ul-ex-write"
                               :queue "ul-q-0"}}
           "ul" {:name "ul" 
                 :password (pw "ul")
                 :queues #{"ul-q-0"}
                 :exchange "ul-ex-write"
                 :allocations {}}}
   :covenants {"dss2" {:from "ul" 
                       :to "tiq" 
                       :tag "storedata"}
               "das2" {:from "ul" 
                       :to "siemens" 
                       :tag "diagnosis"}} 
   :collections {"ALL" #{"dss2" "das2"}}})


(def contracts-zone-tq-area-web
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Tiq, Area Web"
    :aliases ["rabbit@VM-TQ-W-CCU" "10.1.3.2:15673" "192.168.0.10:5672"]
    :management-user "admin"
    :management-password "fV0gXQjWxhH33Humng4q"
    :management-url "http://10.1.3.2:15673"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}
  {:users {"proxy-pf-tq"{:name "proxy-pf-tq" 
                      :password (pw "proxy-pf-tq")
                      :queues #{"proxy-pf-tq-q-0"}
                      :exchange "proxy-pf-tq-ex-write" 
                      :allocations {"dss2" #{"proxy-pf-tq-q-0"}
                                    "das1" #{"proxy-pf-tq-q-0"}}
                      :remote {:aliases #{"ul" "siemens" "psipenta"}
                               :uri "amqp://tiq:tiq@10.1.1.2:15672/VH_tiq"
                               :local-uri "amqp://proxy-pf-tq:proxy-pf-tq@/VH_proxy-pf-tq"
                               :exchange "tiq-ex-write"
                               :queue "tiq-q-0"}}
           "tiq" {:name "tiq" 
                 :password (pw "tiq")
                 :queues #{"tiq-q-0"}
                 :exchange "tiq-ex-write"
                 :allocations {"das1" #{"tiq-q-0"}
                               "dss2" #{"tiq-q-0"}
                               "erp3t" #{"tiq-q-0"}
                               "erp5" #{"tiq-q-0"}
                               "erp6" #{"tiq-q-0"}
                               "erp4t" #{"tiq-q-0"}}}}
   :covenants {"dss2" {:from "ul" 
                       :to "tiq" 
                       :tag "storedata"}
               "das1" {:from "siemens" 
                       :to "tiq" 
                       :tag "storedata"}
               "erp3t" {:from "siemens"
                        :to "tiq"
                        :tag "order"}               
               "erp5" {:from "psipenta" 
                       :to "tiq" 
                       :tag "bill"}
               "erp4t" {:from "psipenta" 
                        :to "tiq" 
                        :tag "report"}
               "erp6" {:from "psipenta" 
                       :to "tiq" 
                       :tag "order"}} 
         :collections {"ALL" #{"dss2" "das1" "erp5" "erp4t" "erp6" "erp3t"}}})

(def contracts-zone-tq-area-app
  ^{:ppu-vhost "VH_ppu" 
    :name "Zone Tiq, Area App"
    :aliases ["rabbit@VM-TQ-A-CCU" "10.1.3.2:25673"]
    :management-user "admin"
    :management-password "fV0gXQjWxhH33Humng4q"
    :management-url "http://10.1.3.2:25673"
    :shovel-user "shovel"
    :shovel-password shovel-pw
    :shovel-password-hash shovel-pw-hash}
  {:users {"proxy-web-tq"{:name "proxy-web-tq" 
                      :password (pw "proxy-web-tq")
                      :queues #{"proxy-web-tq-q-0"}
                      :exchange "proxy-web-tq-ex-write" 
                      :allocations {"dss2" #{"proxy-web-tq-q-0"}
                                    "das1" #{"proxy-web-tq-q-0"}}
                      :remote {:aliases #{"ul" "siemens" "psipenta"}
                               :uri "amqp://tiq:tiq@192.168.0.10:5672/VH_tiq"
                               :local-uri "amqp://proxy-web-tq:proxy-web-tq@/VH_proxy-web-tq"
                               :exchange "tiq-ex-write"
                               :queue "tiq-q-0"}}
           "tiq" {:name "tiq" 
                 :password (pw "tiq")
                 :queues #{"tiq-q-0"}
                 :exchange "tiq-ex-write"
                 :allocations {"das1" #{"tiq-q-0"}
                               "dss2" #{"tiq-q-0"}
                               "erp3t" #{"tiq-q-0"}
                               "erp5" #{"tiq-q-0"}
                               "erp6" #{"tiq-q-0"}
                               "erp4t" #{"tiq-q-0"}}}}
   :covenants {"dss2" {:from "ul" 
                       :to "tiq" 
                       :tag "storedata"}
               "das1" {:from "siemens" 
                       :to "tiq" 
                       :tag "storedata"}
               "erp3t" {:from "siemens"
                        :to "tiq"
                        :tag "order"}
               "erp5" {:from "psipenta" 
                       :to "tiq" 
                       :tag "bill"}
               "erp4t" {:from "psipenta" 
                        :to "tiq" 
                        :tag "report"}
               "erp6" {:from "psipenta" 
                       :to "tiq" 
                       :tag "order"}} 
   :collections {"ALL" #{"dss2" "das1" "erp5" "erp4t" "erp6"}}})

