(ns security-experiments
  (:require [common 
             [proto :refer [protodef parse-protobuf]]
             [rabbit :refer [send-rpc with-connection]]
             [protorabbit :refer [send!]]]
            [flatland.protobuf.core :as pb :refer [protobuf protobuf-dump protobuf-load]]
            [langohr 
             [core      :as rmq]
             [channel   :as lch]
             [queue     :as lq]
             [consumers :as lc]
             [basic     :as lb]]))

(defn test-foo [] 
  (let [conn  (rmq/connect {:uri "amqp://foo:foo@localhost:5672"})
        ch (lch/open conn)]
    (lb/publish ch "foo-exchange" "foo" (.getBytes "test") :persistent true)
    (lc/subscribe ch "foo-queue" (fn [_ meta payload] (println "got msg.:" meta (String. payload))) :auto-ack true)
    (dotimes [_ 1000] (lb/publish ch "foo-exchange" "foo" (.getBytes "test") :persistent true))
    
    ;  (lc/subscribe ch "bar-queue" (fn [_ meta payload] (println "got msg.:" meta (String. payload))))
    (Thread/sleep 1000)
    (rmq/close conn)))

(defn test-bar [] 
  (let [conn  (rmq/connect {:uri "amqp://bar:bar@localhost:5672"})
        ch (lch/open conn)]
    (try
      ; no error? also no effect on the server, fails on next command
;    (lb/publish ch "foo-exchange" "bar" (.getBytes "bar-test") :persistent true) 
;error on subscribe    
;(lc/subscribe ch "foo-queue" (fn [_ meta payload] (println "got msg.:" meta (String. payload))) :auto-ack true)
    (lb/publish ch "bar-exchange" "foo" (.getBytes "test") :persistent true)
    
    ;  (lc/subscribe ch "bar-queue" (fn [_ meta payload] (println "got msg.:" meta (String. payload))))
    (Thread/sleep 2000)
    (finally
      (rmq/close conn)))))

(defn test-winccoa-comos []
  (with-connection [conn-c (rmq/connect {:uri "amqp://comos:5PecTUN4NH23wLAJxxqy@10.1.1.2:15672/pingpong"})
                    conn-w (rmq/connect {:uri "amqp://winccoa:GzZYGVuLlK7FUPsQzi8z@10.1.1.2:15672/pingpong"
                                         })
                    ch-c (lch/open conn-c)
                    ch-w (lch/open conn-w)]
    (lc/subscribe ch-c "comos.data" (fn [_ m payload] (println "got msg. on queue comos.data: " (pr-str m) (String. payload))))
    (lc/subscribe ch-w "winccoa.data" (fn [_ m payload] (println "got msg. on queue winccoa.data: " m (String. payload))))
    (lb/publish ch-c "comos.data" "COMOS.AEB38335-8D44-4A70-823D-33AB1BC6E159.ALL" (.getBytes "comos an winccoa") :persistent false)
    (lb/publish ch-w "winccoa.data" "WINCCOA.AEB38335-8D44-4A70-823D-33AB1BC6E159.ALL" (.getBytes "winccoa an comos") :persistent false)
    (Thread/sleep 2000)))

