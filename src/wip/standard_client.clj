(ns wip.standard-client
  (:require [common.rabbit :refer [with-connection]]
            [clojure.walk]
            [langohr 
             [core      :as rmq]
             [channel   :as lch]
             [queue     :as lq]
             [consumers :as lc]
             [basic     :as lb]]
            [clojure.tools.logging :as log :refer [info warn debug trace infof error]]))

(defn logging-consumer [name ch headers ^bytes payload]
  (infof "[%s] headers='%s', payload='%s'" name (pr-str headers) (pr-str (String. payload "utf8"))))

(defn- remove-nils [m]
  (into {} (keep (fn [[k v]] (when v [k v])) m)))

(defonce tracing-msgs (atom []))

(defn- remove-rmq-internal-classes 
  "There is a java map instance in there with instances of internal private classes etc.
We need to traverse the data structure and convert everything to something more printer friendly.
Look at you, com.rabbitmq.client.impl.LongStringHelper$ByteArrayLongString ...
But since clojure.walk doesn't walk java's data structures, we need to convert them first."
  [m]
  (clojure.walk/prewalk #(cond 
                           (instance? java.util.HashMap %) (into {} %)
                           (instance? java.util.ArrayList %) (into [] %)
                           (.contains (str (class %)) "ByteArray") (str %)
                           :else %) m))

(defn logging-consumer-wo-payload [name vhost ch headers ^bytes payload]
  (let [len (alength payload)
        hdrs (remove-rmq-internal-classes (remove-nils (:headers headers)))
        ^String rk (:routing-key headers)
        idx-dot (.indexOf rk ".")
        action (subs rk 0 idx-dot)
        resource (subs rk (inc idx-dot))
        real-rks (get-in headers [:headers "routing_keys"])] 
    #_(swap! tracing-msgs conj {:name name
                              :action action
                              :resource resource
                              :vhost vhost
                              :payload-length len 
                              :headers hdrs}) 
    (infof "[%s] VHost %s, Action %s, Resource %s: RK=%s, Payload size=%d" name vhost action resource (str real-rks) len #_(pr-str hdrs))))

(defn connect-tracing [{:keys [vhost name] :as settings}] 
  (let [conn (rmq/connect settings)
        ch (lch/open conn)]
    [(lc/subscribe ch 
                   (:queue settings) 
                   #(logging-consumer-wo-payload name vhost % %2 %3)
                   :auto-ack true) 
     conn]))

(def all-descriptors 
  [
   ["amqp://admin:z1sLat202lt3mR7B2Uyn@192.168.0.10:5672/VH_ppu" "Zone UL, Area Web, PPU"]
   ["amqp://admin:z1sLat202lt3mR7B2Uyn@192.168.0.10:5672/VH_ul" "Zone UL, Area Web, VH_ul"]
   ["amqp://admin:z1sLat202lt3mR7B2Uyn@192.168.0.2:25672/VH_ppu" "Zone UL, Area App, PPU"]
   ["amqp://admin:z1sLat202lt3mR7B2Uyn@192.168.0.2:25672/VH_ul" "Zone UL, Area App, VH_ul"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:15672/VH_ppu" "Zone UL, Area Web, PPU"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:15672/VH_foo" "Zone UL, Area Web, VH_foo"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:15672/VH_ul" "Zone UL, Area Web, VH_ul"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:15672/VH_upstream" "Zone UL, Area Web, VH_upstream"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:25672/VH_ppu" "Zone UL, Area App, PPU"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:25672/VH_ul" "Zone UL, Area App, VH_ul"]
;           ["amqp://admin:z1sLat202lt3mR7B2Uyn@10.1.4.2:25672/VH_upstream" "Zone UL, Area App, VH_upstream"]
   ; tiq
   ["amqp://admin:fV0gXQjWxhH33Humng4q@10.1.3.2:15672/VH_ppu" "Zone TQ, Area Web, PPU"]
   ["amqp://admin:fV0gXQjWxhH33Humng4q@10.1.3.2:15672/VH_tiq" "Zone TQ, Area Web, VH_tiq"]
   ["amqp://admin:fV0gXQjWxhH33Humng4q@10.1.3.2:25672/VH_ppu" "Zone TQ, Area App, PPU"]
   ["amqp://admin:fV0gXQjWxhH33Humng4q@10.1.3.2:25672/VH_tiq" "Zone TQ, Area App, VH_tiq"]
   ; platform
   ["amqp://admin:tgsF52Pf5irMiDormEXO@10.1.1.2:15672/VH_ppu" "Zone PF, Area Web, PPU"]
   ["amqp://admin:tgsF52Pf5irMiDormEXO@10.1.1.2:15672/VH_siemens" "Zone PF, Area Web, VH_siemens"]
   ["amqp://admin:tgsF52Pf5irMiDormEXO@10.1.1.2:15672/VH_tiq" "Zone PF, Area Web, VH_tiq"]
   ["amqp://admin:tgsF52Pf5irMiDormEXO@10.1.1.2:15672/VH_psipenta" "Zone PF, Area Web, VH_psipenta"]
   ["amqp://admin:tgsF52Pf5irMiDormEXO@10.1.1.2:15672/VH_ul" "Zone PF, Area Web, VH_ul"]
   ; siemens auf ext. Schrank
   ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.0.0.10:5672/VH_ppu" "Zone Siemens, Area Web, PPU"]
   ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.0.0.10:5672/VH_siemens" "Zone Siemens, Area Web, VH_siemens"]
   ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.0.0.10:5672/VH_winccoa" "Zone Siemens, Area Web, VH_winccoa"]
   ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.0.0.10:5672/VH_comos" "Zone Siemens, Area Web, VH_comos"]
;           ; siemens auf VM
;           ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.1.2.2:15672/VH_ppu" "Zone Siemens, Area Web, PPU"]
;           ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.1.2.2:15672/VH_siemens" "Zone Siemens, Area Web, VH_siemens"]
;           ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.1.2.2:15672/VH_proxyuser" "Zone Siemens, Area Web, VH_proxyuser"]
;           ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.1.2.2:15672/VH_winccoa" "Zone Siemens, Area Web, VH_winccoa"]
;           ["amqp://admin:pDBQd7uS6llbDwYFOWzF@10.1.2.2:15672/VH_comos" "Zone Siemens, Area Web, VH_comos"]

           ])

(defonce ^:private conns (atom []))

(defn connection-status []
  (map (comp rmq/open? second) @conns))

(defn close-all-connections! []  
  (->> @conns
    (map second)
    (remove #(or (nil? %) (rmq/closed? %)))
    (map #(.close %))
    dorun) )

(defn open-all-connections! [connection-descriptors]
  (reset! conns
          (mapv (fn [[uri name]] 
                  (println "connecting to " name "-")
                  (try 
                    (connect-tracing (merge {:uri uri
                                             :name name
                                             :queue "admin.tracing.queue" 
                                             :automatically-recover true}
                                            (rmq/settings-from uri)))
                    (catch Exception e
                      (.printStackTrace e)
                      (error "could not connect to" name e))))
                connection-descriptors))) 
(comment
    

  
  
  (with-connection [conn (rmq/connect {:uri "amqp://winccoa:winccoa@10.1.2.2:15672/VH_winccoa"})
                    ch (lch/open conn)]
    (lb/publish ch "winccoa-ex-write" "siemens.storedata.UL" (.getBytes "test")))
  )